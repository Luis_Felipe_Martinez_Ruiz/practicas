package Practica_3_Calculadora;

import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/**
 *
 * @author felipe
 */
public class Calculadora extends JFrame {

    JPanel panelTop, panelDown;
    JTextField pantalla;
    JButton btn1, btn2, btn3, btn4, btn5, btn6,
            btn7, btn8, btn9, btn0, btn11, btn12, btn13, btn14, btn15, btn16;

    public Calculadora() {
        panelTop();
        panelDown();
        ventana();
    }

    void panelTop() {
        panelTop = new JPanel();
        panelTop.setLayout(null);
        pantalla = new JTextField();
        pantalla.setSize(400, 60);
        pantalla.setHorizontalAlignment(JTextField.RIGHT);
        pantalla.setVisible(true);
        panelTop.add(pantalla);
    }

    void panelDown() {
        panelDown = new JPanel();
        panelDown.setLayout(new GridLayout(4, 4));
        btn1 = new JButton("1");
        btn2 = new JButton("2");
        btn3 = new JButton("3");
        btn4 = new JButton("4");
        btn5 = new JButton("5");
        btn6 = new JButton("6");
        btn7 = new JButton("7");
        btn8 = new JButton("8");
        btn9 = new JButton("9");
        btn0 = new JButton("0");
        btn11 = new JButton("+");
        btn12 = new JButton("-");
        btn13 = new JButton("*");
        btn14 = new JButton("/");
        btn15 = new JButton("=");
        btn16 = new JButton(".");

        panelDown.add(btn7);
        panelDown.add(btn8);
        panelDown.add(btn9);
        panelDown.add(btn14);
        panelDown.add(btn4);
        panelDown.add(btn5);
        panelDown.add(btn6);
        panelDown.add(btn13);
        panelDown.add(btn1);
        panelDown.add(btn2);
        panelDown.add(btn3);
        panelDown.add(btn12);
        panelDown.add(btn15);
        panelDown.add(btn0);
        panelDown.add(btn16);
        panelDown.add(btn11);
    }

    void ventana() {
        this.setTitle("Calculadora");
        this.setSize(400, 300);
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));//BoxLayout.Y_Axis hace 
                                                                               //que el acomodo se en 
                                                                               //vertical
        this.add(panelTop);
        this.add(panelDown);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        Calculadora cal = new Calculadora();
    }

}
