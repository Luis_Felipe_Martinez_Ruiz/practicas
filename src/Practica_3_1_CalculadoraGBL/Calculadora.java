package Practica_3_1_CalculadoraGBL;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author felipe
 */
public class Calculadora extends JFrame {

    JPanel panel;
    JTextField pantalla;
    JButton btn1, btn2, btn3, btn4, btn5, btn6,
            btn7, btn8, btn9, btn0, btn11, btn12, btn13, btn14, btn15, btn16;
    GridBagConstraints gbc;

    public Calculadora() {
        panel();
        ventana();
    }

    void panel() {
        panel = new JPanel();
        gbc = new GridBagConstraints();//encargado de manejar
                                       //las propiedades del
                                       //gridbaglayout
        panel.setLayout(new GridBagLayout());
        btn1 = new JButton("1");
        btn2 = new JButton("2");
        btn3 = new JButton("3");
        btn4 = new JButton("4");
        btn5 = new JButton("5");
        btn6 = new JButton("6");
        btn7 = new JButton("7");
        btn8 = new JButton("8");
        btn9 = new JButton("9");
        btn0 = new JButton("0");
        btn11 = new JButton("+");
        btn12 = new JButton("-");
        btn13 = new JButton("*");
        btn14 = new JButton("/");
        btn15 = new JButton("=");
        btn16 = new JButton(".");
        pantalla = new JTextField();
        pantalla.setHorizontalAlignment(JTextField.RIGHT);//alinea el textfield
                                                          //a la derecha
                                                          
        //botones                                        
                                                          
        //gridx y gridy son coordenadas
        //de cada uno de los componentes
        //en cuadricula
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(btn7, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        panel.add(btn8, gbc);

        gbc.gridx = 2;
        gbc.gridy = 1;
        panel.add(btn9, gbc);

        gbc.gridx = 3;
        gbc.gridy = 1;
        panel.add(btn14, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(btn4, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        panel.add(btn5, gbc);

        gbc.gridx = 2;
        gbc.gridy = 2;
        panel.add(btn6, gbc);

        gbc.gridx = 3;
        gbc.gridy = 2;
        panel.add(btn13, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        panel.add(btn1, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        panel.add(btn2, gbc);

        gbc.gridx = 2;
        gbc.gridy = 3;
        panel.add(btn3, gbc);

        gbc.gridx = 3;
        gbc.gridy = 3;
        panel.add(btn12, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        panel.add(btn15, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        panel.add(btn0, gbc);

        gbc.gridx = 2;
        gbc.gridy = 4;
        panel.add(btn16, gbc);

        gbc.gridx = 3;
        gbc.gridy = 4;
        panel.add(btn11, gbc);
        
        //textfield
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        gbc.ipadx = 300;//modifica el padding interno en x
        gbc.ipady = 9;//modifica el padding interno en y
        panel.add(pantalla, gbc);    
    }

    void ventana() {
        this.setTitle("Calculadora");
        this.setSize(310, 230);
        this.setLocationRelativeTo(null);//Centra la ventana
        this.add(panel);
        this.setVisible(true);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        Calculadora cal = new Calculadora();
    }

}
