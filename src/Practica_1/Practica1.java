/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Practica_1;

import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;

import java.awt.FlowLayout;

/**
 *
 * @author gmendez
 */
public class Practica1 extends JFrame { // implements ActionListener {

    JButton btnCerrar;
    JTextField tfNombre;

    public Practica1() {
        this.setSize(640, 480);
        this.setTitle("Practica 1");
        this.setLayout(new FlowLayout());

        // Con la siguiente linea, ya no es necesario implementar la
        // interfaz WindowListener
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        btnCerrar = new JButton("Cerrar");
        btnCerrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Desde metodo en clase anonima");
                System.exit(0);
            }
        });

        tfNombre = new JTextField(30);

        this.add(tfNombre);
        this.add(btnCerrar);

        this.setVisible(true);
    }

    public static void main(String args[]) {
        Practica1 pract1 = new Practica1();
    }
}

class MiListener extends WindowAdapter implements ActionListener {

    void cerrar() {
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Se presionó el boton cerrar");
        cerrar();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Cerrandose la ventana");
        cerrar();
    }
}
