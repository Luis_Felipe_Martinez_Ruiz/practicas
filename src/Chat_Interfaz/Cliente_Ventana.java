package Chat_Interfaz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class Cliente_Ventana extends javax.swing.JFrame implements Runnable {

    String usuario;
    DefaultListModel defaultListModel = new DefaultListModel();
    BufferedReader in;
    PrintWriter out;
    Socket cnx;
    boolean ejecutar = true;

    public Cliente_Ventana() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        defaultListModel = new DefaultListModel();
        usuarios.setModel(defaultListModel);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Mensaje = new javax.swing.JTextField();
        enviar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        usuarios = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        in_msg = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Mensaje.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        Mensaje.setText("Escriba su mensaje ");
        Mensaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MensajeMouseClicked(evt);
            }
        });
        Mensaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MensajeActionPerformed(evt);
            }
        });

        enviar.setText("Enviar");
        enviar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                enviarMouseClicked(evt);
            }
        });
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });

        usuarios.setToolTipText("");
        jScrollPane1.setViewportView(usuarios);

        jLabel1.setText("Usuarios");

        jLabel2.setText("CHAT");

        in_msg.setColumns(20);
        in_msg.setRows(5);
        jScrollPane3.setViewportView(in_msg);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Mensaje, javax.swing.GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(enviar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(213, 213, 213)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 338, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(97, 97, 97))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(enviar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Mensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enviarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_enviarMouseClicked
    }//GEN-LAST:event_enviarMouseClicked

    //enviamos el mensaje 
    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        String mensaje = Mensaje.getText();
        if (!mensaje.equalsIgnoreCase("salir")) {
            this.send(mensaje);
            Mensaje.setText("");
            in_msg.setText(in_msg.getText() + usuario + ": " + mensaje + "\n");
        } else {
            send(mensaje);
            Mensaje.setText("");
            try {
                ejecutar = false;
                in.close();
                cnx.close();
                out.close();
                System.exit(0);
            } catch (IOException ex) {
                Logger.getLogger(Cliente_Ventana.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_enviarActionPerformed

    private void MensajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MensajeActionPerformed

    }//GEN-LAST:event_MensajeActionPerformed

    private void MensajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MensajeMouseClicked
        Mensaje.setText("");
    }//GEN-LAST:event_MensajeMouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cliente_Ventana().run();
            }
        });

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Mensaje;
    private javax.swing.JButton enviar;
    private javax.swing.JTextArea in_msg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList<String> usuarios;
    // End of variables declaration//GEN-END:variables

    void start() {
        Conexion hilo;
        try {
            this.cnx = new Socket("localhost", 4444);
            in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
            out = new PrintWriter(cnx.getOutputStream(), true);

            hilo = new Conexion(in);
            hilo.start();

        } catch (IOException ex) {
            Logger.getLogger(Cliente_Ventana.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void send(String mensaje) {
        out.println(mensaje);
    }

    @Override
    public void run() {
        start();
        this.setVisible(true);
    }

    class Conexion extends Thread {

        BufferedReader in;

        public Conexion(BufferedReader in) {
            this.in = in;
        }

        @Override
        public void run() {
            String respuesta = "";
            boolean login = false;
            while (ejecutar) {
                try {

                    respuesta = in.readLine();

                    String user = "";

                    if (respuesta != null) {

                        while (!login && cnx.isConnected()) {
                            if (respuesta.equals("Bienvenido, proporcione su usuario")) {
                                user = JOptionPane.showInputDialog("Ingrese su nombre");
                                out.println(user);
                            } else if (respuesta.equals("Escriba el password")) {
                                String pass = JOptionPane.showInputDialog("Ingrese su contraseña");
                                out.println(pass);
                            } else {
                                login = !login;
                                usuario = user;
                                in_msg.setText(in_msg.getText() + respuesta + "\n");
                                respuesta = in.readLine();
                                addUsers(respuesta);
                            }
                            respuesta = in.readLine();
                        }

                        in_msg.setText(in_msg.getText() + respuesta + "\n");

                        if (respuesta != null && respuesta.contains("Se ha unido a la conversacion.")) {
                            defaultListModel.addElement(respuesta.substring(0, respuesta.indexOf("Se ha") - 1));
                        } else if (respuesta != null && respuesta.contains("ha salido del chat.")) {
                            defaultListModel.removeElement(respuesta.substring(0, respuesta.indexOf("ha salido") - 1));
                        }
                    } else {

                    }

                } catch (IOException ex) {
                    Logger.getLogger(Cliente_Ventana.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void addUsers(String users) {
        users = users.substring(1, users.length() - 1);//leemos los usuarios
        int len = users.length();
        while (len != 0) {//mientras la cadena que contiene usuarios no este vacia   
            if (users.contains(",")) {
                defaultListModel.addElement(users.substring(0, users.indexOf(",")));//añadiamos a la lista de usuarios el string que contiene el nombre hasta la primer coma
                users = users.substring(users.indexOf(",") + 1, len); //recortamos el string para quitar el nombre ya agregado
                len = users.length();//actualizamos el valor
            } else {
                defaultListModel.addElement(users);
                len = 0;
            }
        }
    }
}
