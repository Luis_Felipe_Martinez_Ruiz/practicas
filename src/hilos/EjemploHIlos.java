/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luisf
 */
public class EjemploHIlos {
    public static void main(String[] args) {
        Thread th1 = new Thread(new HiloRunnable());
        Thread th2 = new HiloThread();
            
        th1.start(); th2.start();
       
        try {
            th1.join();
            th2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(EjemploHIlos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

class HiloRunnable implements Runnable{

    @Override
    public void run() {
        
        for (int i = 0; i < 20; i++) {
            System.out.println("Buenos dias: " + i);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Termino el hilo HiloRunnable");
    }
}

class HiloThread extends Thread{
    
      @Override
    public void run() {
        
        for (int i = 100; i < 120; i++) {
            System.out.println("Buenos noches: " + i);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Termino el hilo HiloThread");
    }
}
